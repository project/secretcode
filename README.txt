This module allows an admin to create a secret code that users must enter
to register their accounts to. It is particularly useful for cases when
it's impractical for the website administrator to manually process, but still
important to control exactly who has access to certain roles.

To install, visit admin/build/modules and activate the
module.

IMPORTANT: the module does not provide an upgrade path from
version 5 nor from 6.x-1.x.

The settings page is at admin/user/secretcode.
